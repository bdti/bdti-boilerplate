# BDTI Boilerplate

Welcome to our platform repository! This document serves as a guide to the organized structure of directories and tools provided. Each folder in this repository has a specific purpose, designed to streamline development processes and enhance productivity.

## Repository Structure

Below is the hierarchy of the directory structure, including descriptions of each folder's intended use:

- `data`: This directory should contain all the datasets or data dumps used in your projects.
- `docs`: Store all your project documentation here, including manuals, project requirements, and reference guides.
- `references`: Use this folder for storing reference materials such as papers, external articles, or any third-party documentation.
- `reports`: This directory is intended for saving analysis reports, generated metrics, and other output files that summarize findings.
- `tools`: This folder houses various BDTI tools categorized by their utility. Each subdirectory contains specific tools relevant to your workflows.

### Tools Directory Breakdown

The `tools` directory is further divided into specialized subdirectories:

#### Advanced Processing

- `Apache Spark`: For large-scale data processing.
- `Elasticsearch`: For searching and analyzing indexed data.
- `Kibana`: Visualization frontend for data indexed in Elasticsearch.

#### Data Lake

- `MinIO`: High-performance, Kubernetes-native object storage.

#### Databases

- `MongoDB`: NoSQL database for handling document-oriented information.
- `PostgreSQL`: Advanced open-source relational database, with integrated `postgis` (open-source spatial database extender for PostgreSQL, enabling it to store, query, and manage geographic and location-based data.).
- `Virtuoso`: Hybrid database for both SQL and NoSQL data management.

#### Development Environments

- `H2O.ai`: An open-source data analysis tool.
- `JupyterLab`: Interactive development environment for notebooks.
- `KNIME`: Workflow platform for data-driven innovation.
- `Rstudio`: IDE for R language.
- `VS Code (With integrated Python or R)`:  Is a free, open-source code editor developed by Microsoft, known for its versatility and extensive features for programming and debugging.
- `Doccano`: Is an open-source text annotation tool used for labeling datasets for natural language processing (NLP) tasks.

#### GEO Tools

- `QGIS`: Is an open-source geographic information system (GIS) application used for creating, editing, visualizing, and analyzing geospatial data.

#### Orchestration

- `Apache Airflow`: Tool to manage and schedule data pipelines.
- `Mage AI`: Data preparation and transformation workflows automation.

#### Visualization

- `Apache Superset`: Tool for data exploration and visualization.
- `Metabase`: Simple, powerful tool for data visualization.

## Usage Guidelines

- Ensure that you keep your data organized according to the structure specified.
- Regularly update your documentation to reflect any changes or updates in your project.
- Utilize the tools provided in the respective directories for consistency and to maintain the integrity of your workflows.

## Version Control Best Practices

- Commit early and often: Small, frequent commits are preferable.
- Write meaningful commit messages: Clearly describe what has been changed and why.
- Use branches for features, bug fixes, and experiments: This keeps the main branch clean and deployable.

If you need to know more about GIT, see the following [official tutorial.](https://git-scm.com/docs/gittutorial)

We hope this guide helps you to efficiently utilize the repository and the tools provided. Happy coding on BDTI Platform!
